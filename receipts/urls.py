from django.urls import path
from receipts.views import receipt_home, create_receipt, categories, accounts, category_create, account_create

urlpatterns = [
    path("", receipt_home, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", categories, name="categories"),
    path("accounts/", accounts, name="accounts"),
    path("categories/create/", category_create, name="category_create"),
    path("accounts/create/", account_create, name="account_create")
]
