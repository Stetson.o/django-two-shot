from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
# Create your views here.
@login_required
def receipt_home(request):
    list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_object": list,
    }
    return render(request, "receipts/home.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            buyer = form.save(False)
            buyer.purchaser = request.user
            buyer.save()
            return redirect("home")
    else:
        form = ReceiptForm
    context = {
        'form': form,
    }
    return render(request, "receipts/create_receipt.html", context)

@login_required
def categories(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_object": category,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def accounts(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account_object": account,
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def category_create(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            person = form.save(False)
            person.owner = request.user
            person.save()
            return redirect("categories")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form
    }
    return render(request, "receipts/categories/create_category.html", context)

@login_required
def account_create(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            person = form.save(False)
            person.owner = request.user
            person.save()
            return redirect("accounts")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "receipts/accounts/create_account.html", context)
